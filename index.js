var server = require('./modules/server');
var mosca = require('mosca');

var mongo = {
    type: 'mongo',
    url: 'mongodb://localhost:27017/mqtt',
    pubsubCollection: 'toranj',
    mongo: {}
};

var config = {
    port: 7734,
    backend: mongo,
    persistence: {
        factory: mosca.persistence.Mongo,
        url: 'mongodb://localhost:27017/mqtt'
    }

};

server(config);
