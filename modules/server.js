var mosca = require('mosca');
var bunyan = require('bunyan');
var bunyanConfig = require('./bunyanConfig');
var chalk = require('chalk');

var log = bunyan.createLogger(bunyanConfig);

chalk.enabled = true;

var server = function(config) {
    var moscaServer = new mosca.Server(config);

    moscaServer.on('ready', function() {
        log.info('Mosca server activated at port ' + chalk.yellow(config.port) +
            ' with ' + chalk.yellow(config.backend.type) + ' persistence.');
    });

    moscaServer.on('clientConnected', function(client) {
        log.info({
            client: chalk.cyan(client)
        }, 'Client connected.');
    });

    moscaServer.on('clientDisconnected', function(client) {
        log.info({
            client: chalk.cyan(client)
        }, 'Client disconnected.');
    });

    moscaServer.on('published', function(packet) {
        log.info({
            packet: chalk.yellow(JSON.stringify(packet))
        }, 'Client published.');
    });
};

module.exports = server;
